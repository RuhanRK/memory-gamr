// Page Loading Section  ----------
var initialInput = document.getElementById('input');
$(function(){
   
    var welcomeSection = $('.page-load ');
    var enterButton = $('.enter-button');
        setTimeout(function(){
          welcomeSection.removeClass('content-hidden');
        }, 800);

        enterButton.on('click', function(e){

          var result = initialInput.value;
          if(result == ""){
            enterButton.attr("href", "#modal");
          }
          else{
            e.preventDefault();
            $(".content-wrap").animate({top: "-400px"})
            $(".content-wrap1").animate({left: "-600px"});
            $(".content-wrap2").animate({right: "-600px"});

            welcomeSection.fadeOut(3000, "swing");
          }
        });
        // trigger Enter Key
        initialInput.addEventListener("keyup", function(event){
            var result = initialInput.value;
            event.preventDefault();
            if (event.keyCode === 13 && result != ""){
                $(".content-wrap").animate({top: "-400px"})
                $(".content-wrap1").animate({left: "-600px"});
                $(".content-wrap2").animate({right: "-600px"});
                welcomeSection.fadeOut(3000, "swing");
            }
            else if(event.keyCode === 13 && result == ""){
                alert("Please Enter Yourname Before Unlock this game");
            }
        });
  });

let card = document.getElementsByClassName("card");
let cards = [...card]; // holds all cards


const deck = document.getElementById("card-deck");

let moves = 0;
let counter = document.querySelector(".moves");

const stars = document.querySelectorAll(".fa-star");

let matchedCard = document.getElementsByClassName("match");

 let starsList = document.querySelectorAll(".stars li");

 let closebtn = document.querySelector(".close");

 let modal = document.getElementById("c_modal");

var openedCards = [];


function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};


// shuffle cards when page is refreshed
document.body.onload = startGame();

// start a new game ----------------
function startGame(){
    cards = shuffle(cards);
    for (var i = 0; i < cards.length; i++){
        deck.innerHTML = "";
        [].forEach.call(cards, function(item) {
            deck.appendChild(item);
        });
        cards[i].classList.remove("show", "open", "match", "disabled");
    }
    // reset moves
    moves = 0;
    counter.innerHTML = moves;
    // reset rating
    for (var i= 0; i < stars.length; i++){
        stars[i].style.color = "#435f6f";
        stars[i].style.visibility = "visible";
    }
    //reset timer
    second = 0;
    minute = 0;
    var timer = document.querySelector(".timer");
    timer.innerHTML = "0 mins 0 secs";
    clearInterval(interval);
}


// Toggling for display cards ------------------
var displayCard = function (){
    this.classList.toggle("open");
    this.classList.toggle("show");
    this.classList.toggle("disabled");
};


//  Add opened cards to OpenedCards list and check if cards are match or not--------------------
function cardOpen() {
    openedCards.push(this);
    var len = openedCards.length;
    if(len === 2){
        moveCounter();
        if(openedCards[0].type === openedCards[1].type){
            matched();
        } else {
            unmatched();
        }
    }
};



//Function for Card Matched--------------------
var matchSounds = document.getElementById('matchSound');
function mSound(){
    matchSounds.play();
}
function matched(){
    openedCards[0].classList.add("match", "disabled");
    openedCards[1].classList.add("match", "disabled");
    openedCards[0].classList.remove("show", "open");
    openedCards[1].classList.remove("show", "open");
    openedCards = [];
    mSound();
}



// Function for card unmatched-----------------
var umantchs = document.getElementById('umantched');
function uSound(){
    umantchs.play();
}
function unmatched(){
    openedCards[0].classList.add("unmatched");
    openedCards[1].classList.add("unmatched");
    disable();
    setTimeout(function(){
        openedCards[0].classList.remove("show", "open", "unmatched");
        openedCards[1].classList.remove("show", "open", "unmatched");
        enable();
        openedCards = [];
    },600);
    uSound();
}


// Disable cards
function disable(){
    Array.prototype.filter.call(cards, function(card){
        card.classList.add('disabled');
    });
}


// Enable cards and disable matched cards
function enable(){
    Array.prototype.filter.call(cards, function(card){
        card.classList.remove('disabled');
        for(var i = 0; i < matchedCard.length; i++){
            matchedCard[i].classList.add("disabled");
        }
    });
}


// Count for player's moves
function moveCounter(){
    moves++;
    counter.innerHTML = moves;
    //start timer on first click
    if(moves == 1){
        second = 0;
        minute = 0;
        startTimer();
    }
    // setting rates based on moves
    if (moves > 12 && moves <= 14){
        for( i= 0; i < 4; i++){
            if(i > 2){
                stars[i].style.visibility = "collapse";
            }
        }
    }
    else if (moves > 14 && moves < 18){
        for( i= 2; i < 4; i++){
            if(i > 0){
                stars[i].style.visibility = "collapse";
            }
        }
    }
    else if (moves > 18){
        for( i= 0; i < 4; i++){
            if(i > 0){
                stars[i].style.visibility = "collapse";
            }
        }
    }
}


// Game timer-------------
var second = 0, minute = 0;
var timer = document.querySelector(".timer");
var interval;
function startTimer(){
    interval = setInterval(function(){
        timer.innerHTML = minute+"mins "+second+"secs";
        second++;
        if(second == 60){
            minute++;
            second=0;
        }
    },1000);
}

function pauseTime(){
    clearInterval(interval);
    deck.classList.add("deck-disabled");
}
function playTime(){
    startTimer(interval);
    deck.classList.remove("deck-disabled");
}
$('.restart').click(function(){
    deck.classList.remove("deck-disabled");
})
// Congratulations fucntion----------
var congrats = document.getElementById('myCongrats');
function congSound(){
    congrats.play();
};

function congratulations(){
    if (matchedCard.length == 16){
        congSound();
        clearInterval(interval);
        finalTime = timer.innerHTML;

        // show congratulations modal
        modal.classList.add("show");

        // declare star rating variable
        var starRating = document.querySelector(".stars").innerHTML;
        // result
        var result = (100 - moves) /(minute + (second / 60));
        result = Math.round(result * 10);
        //showing move, rating, time on modal
        document.getElementById("totalMove").innerHTML = moves;
        document.getElementById("starRating").innerHTML = starRating;
        document.getElementById("totalTime").innerHTML = finalTime;
        document.getElementById('my_score').innerHTML = result;
        var submitButton = document.getElementById('submits');
        var initialinput = document.getElementById('input');
        
        
        submitButton.onclick=function(){
            submitScore();
            modal.classList.remove("show");
            startGame();
            submitButton.href = "#modal-submit";
        };
        var database = firebase.database();
        function submitScore(){
          var data = {
            name: initialinput.value,
            score: result
          }
          console.log(data);
          var ref = database.ref('scores');
          ref.push(data);
        }

        //closebtn =
        closeModal();
    };
}
// Close icon on modal--------------
function closeModal(){
    closebtn.addEventListener("click", function(e){
        modal.classList.remove("show");
        startGame();
    });
}

//For user to play Again ---------------
function playAgain(){
    modal.classList.remove("show");
    startGame();
}


// loop to add event listeners to each card------------
for (var i = 0; i < cards.length; i++){
    card = cards[i];
    card.addEventListener("click", displayCard);
    card.addEventListener("click", cardOpen);
    card.addEventListener("click",congratulations);
};

// Audio------------
var myAudio = document.getElementById("myAudio");
var isPlaying = false;

function togglePlay() {
  if (isPlaying) {
    myAudio.pause()
  } else {
    myAudio.play();
  }
};
myAudio.onplaying = function() {
  isPlaying = true;
};
myAudio.onpause = function() {
  isPlaying = false;
};
$('.light-modal-footer a').click(function(){
    $(this).find('i').toggleClass('fa-music fa-pause')
});


// Mute System------------

/*!
 * jQuery Double Tap Plugin.
 *
 * Copyright (c) 2010 Raul Sanchez (http://www.appcropolis.com)
 *
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */

 // jQuery Double Tab Function
(function($){
    // Determine if we on iPhone or iPad
    var isiOS = false;
    var agent = navigator.userAgent.toLowerCase();
    if(agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0){
           isiOS = true;
    }

    $.fn.doubletap = function(onDoubleTapCallback, onTapCallback, delay){
        var eventName, action;
        delay = delay == null? 500 : delay;
        eventName = isiOS == true? 'touchend' : 'click';

        $(this).bind(eventName, function(event){
            var now = new Date().getTime();
            var lastTouch = $(this).data('lastTouch') || now + 1 /** the first time this will make delta a negative number */;
            var delta = now - lastTouch;
            clearTimeout(action);
            if(delta<500 && delta>0){
                if(onDoubleTapCallback != null && typeof onDoubleTapCallback == 'function'){
                    onDoubleTapCallback(event);
                }
            }else{
                $(this).data('lastTouch', now);
                action = setTimeout(function(evt){
                    if(onTapCallback != null && typeof onTapCallback == 'function'){
                        onTapCallback(evt);
                    }
                    clearTimeout(action);   // clear the timeout
                }, delay, [event]);
            }
            $(this).data('lastTouch', now);
        });
    };
})(jQuery);

var sounds = document.getElementsByTagName("audio");

$('#mute').doubletap(
    /** doubletap-dblclick callback */
    function(event){
        for (var i = 0; i < sounds.length; ++i) {

    		sounds[i].muted = false;
    }
      $('#mute').text('Mute')

    },
    /** touch-click callback (touch) */
    function(event){
        for (var i = 0; i < sounds.length; ++i) {

    		sounds[i].muted = true;
    }

      $('#mute').text('Double click to unmute')
    },

    500
);


// Social Icon Effect
$(document).ready(function(){
    $('.nav-icon').on('click', function(){
      $('nav ul').toggle();
    });
    $(".nav-icon").mouseover(function(){ $(this).find(".user").animate({marginLeft:'10px'},'slow') });
    $(".nav-icon").mouseleave(function(){ $(this).find(".user").animate({marginLeft:'20px'}, 0) });
  });
  