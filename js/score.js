// Initialize Firebase
var config = {
   apiKey: "AIzaSyDnwOdbUzAo9hjqB1HnVieiQToGzc7zaDo",
   authDomain: "memory-score.firebaseapp.com",
   databaseURL: "https://memory-score.firebaseio.com",
   projectId: "memory-score",
   storageBucket: "",
   messagingSenderId: "916600864459"
 };
 firebase.initializeApp(config);
 
var init = function(){
   
 var options2={
     // customize the following:
     // syntax is like css, but make sure:
     // key and value are both strings!
     "scoreboardWidth":"50%", //recommended
     "scoreboardHeight":"400", // recommended
     "submitDialogWidth":"auto", //recommended
     "submitDialogHeight":"auto", // recommended
          burey:{
     // Scoreboard Module By Burey text
          "font-family":"Times New Roman",
          "font-style":"italic",
          "font-size": "24px",
          "text-align":"center",
          "color": "#fff",
          "border-radius": "25px",
          "border-bottom": "2px solid red"
          },
          dialogTitle:{
            // dialogs title bar
            "box-shadow":"inset 0 -1px 1px #9daabb,inset 0 1px 0.125em #0f1110",
            // "text-shadow": "0px 0px 1px #e0d0ff",
            'border':'1px solid #7710bb',
            "border-radius":"2%",
            "font-family":"'Share Tech Mono', monospace",
            "text-align":"center",
            "background":"linear-gradient(to right top, #474747, #202020)",
            "color":"#fff",
            "font-size": "24px"
            },
          scoreboardContainer:{
     // scoreboard container dialog
         "border":"1px solid grey",
          //"border-radius":"1%",
          "box-shadow":"inset 0 -1px 1px #9daabb,inset 0 1px 0.125em #0f1110",
         "background":"linear-gradient(to right top, #474747, #202020)"
          },
          tableHeader:{
            // # Name Score Time
                 "border-radius":"10px",
                 "font-family":"Times New Roman",
                 "text-align":"center",
                 "box-shadow":"0px 2px 2px #ccc, inset 0px 0px 3px white",
                 "color": "#fff",
                 "margin-top": "10px"
                 },
          scorePosition:{
     // #
          "font-family":"Times New Roman",
          "color": "#fff"
          },
          scoreName:{
     // name of user
          "font-family":"Times New Roman",
          "color": "#fff",
          "word-wrap": "break-word",
          "max-width":"110px",
          "padding-right": "10px"
          },
          scoreValue:{
     // score value
          "font-family":"Times New Roman",
          "font-size": "15px",
          "color": "#fff"
          },
          newScoreContainer:{
     // submit new score dialog
          "background-color":"white"
          },
          scoreYourScoreLabel:{
     // 'Your Score:' text in new score dialog
          "font-family":"Times New Roman",
          "color": "#fff"
          },
          scoreValueLabel:{
     // score value in new score dialog
          "font-family":"Times New Roman",
          "color": "#fff",
          "margin-right":"25px"
          },
          scoreErrorLabel:{
     // error label in new score dialog
          "font-family":"Times New Roman",
          "color": "red"
          },
          scoreboardButtons:{
     // scoreboard dialog buttons
          "font-family":"Times New Roman",
          "text-shadow": "5px 2px 4px grey",
          "background":"black",
          "color": "#fff",
          "display":"block" // remove button
          },
          newScoreButtons:{
     // new score dialog buttons
         "font-family":"Times New Roman",
          "text-shadow": "5px 2px 4px grey",
          "background":"black",
          "color": "#fff"
          },
          dialogButtonPanels:{
          "font-family":"Times New Roman",
          "text-shadow": "5px 2px 4px grey",
          "background":"black",
          "color": "#fff"
          },
          sortDropDownList:{
             "font-family":"Times New Roman",
             "font-size":"20px",
             "margin-left":"5px"
          },
          loaderOptions:{
      // scoreboard loading animation
         "border-bottom": "25px solid #888", /* Light grey */
         "border-top": "25px solid #888", /* Light grey */
         "border-right": "25px solid #9910ff",
         "border-left": "25px solid #9910ff",
         "margin-left": "auto",
         "margin-right": "auto"
     }
 };

     var scoreboard = null;
    $(document).ready(function() {
        // load the scoreboard only when done loading the script!!!
       scoreboard = new Scoreboard(options2);
    });


   document.getElementById('show_scoreboard').onclick=function(){
      scoreboard.showScoreBoard();
  };


     var rules={
             "rules": {
               ".read": false,
               ".write": false,
               "scores": {
                 ".read": true,
                 ".indexOn": ["score"],
                 "$score": {
                   ".write":"newData.exists() || !newData.exists()",
                   ".validate": "newData.hasChildren(['name', 'score', 'time'])",
                   "name": {
                     ".validate": "newData.isString() && newData.val().length <= 30"
                     },
                     "score": {
                       ".validate": "newData.isNumber()"
                     },
                     "time": {
                       ".validate": "newData.isNumber() && newData.val() <= now"
                     }
                   }
                 }
             }
         };
 };

 $(init);

