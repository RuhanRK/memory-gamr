# Welcome to The Avengers Memory Game!

I made this Game for  **Google Udacity Scholarship Challenge 3**. Before Play this game Read This Desccription First.  **Thank you** :heart_eyes:


### Rule of Avengers

Be Careful Before  **Unlock** this game. :yum:  
- Before Unlock this Game you have to write your Good Name (This name will appear in Scoreboard),
- Enter Your Name and Then Press Enter from Your Keyboard or Click **Unlock** button.
- If you want to **Unlock** Without Your _Name_ Then A modal Will popup. :open_mouth:
- Now you Can play This Awesome Avengers Game :joy:

### Features of this Game\

- Avenger icon for Favicon
- Awesome **Unlock** System
- Every time cards will change place
- Cool Sound Effect for **Matched** **Unmatched** and **Congratulation**
- Star Rating With Moves Count
- _Refresh_ whole Game
- _Pause/Relax_ Mode
- _Music_ Option
- _Awrsome Setting_ option
- _Help_ Option
- _Scoreboard_ option
- Dynamic Social Icon Option.



### Help For Game

- Click **setting** icon And you’ll see 
> - **Info** Button -> Here you can see some Brief Intorduction for This game
> - **Prize** Icon -> Here You can see Full Scoreboard
> - **Music** Icon-> You Can Play and Pause Music
> - **Help** Icon-> Here You will assist for Playing Game. 


## How to Play

-   A user must select 2 cards at a time.
-   Mismatched cards will be turned over, and the user must select a new pair of cards.
- -   The player is given a score of 1 star to 3 stars depending on how many moves are Player made
-   Once all 8 pairs are matched, the user is prompted Congratulation Modal..
-   An then Submit your Score By Click **Submit** Button. This Button Will show up in **Congratulation** Modal.




Now Enjoy This Game . _Click on **[Play](https://ruhanrk.gitlab.io/memory-gamr)** Link_ :blush:
